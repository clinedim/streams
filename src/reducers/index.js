import { combineReducers } from "redux";
import { reducer as form } from "redux-form";

import authentication from "./authentication";
import streams from "./streams";

export default combineReducers({ authentication, form, streams });
