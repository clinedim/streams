import { SIGN_IN, SIGN_OUT } from "./types";

export const signIn = (userId) => ({ payload: userId, type: SIGN_IN });

export const signOut = () => ({ type: SIGN_OUT });
