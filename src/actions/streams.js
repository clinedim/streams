import streams from "../apis/streams";
import history from "../history";
import {
  CREATE_STREAM,
  DELETE_STREAM,
  EDIT_STREAM,
  FETCH_STREAM,
  FETCH_STREAMS,
} from "./types";

export const createStream = (stream) => async (dispatch, getState) => {
  const { userId } = getState().authentication;
  const response = await streams.post("", { ...stream, userId });

  dispatch({
    payload: response.data,
    type: CREATE_STREAM,
  });
  history.push("/");
};

export const deleteStream = (id) => async (dispatch) => {
  await streams.delete(`/${id}`);

  dispatch({
    payload: id,
    type: DELETE_STREAM,
  });
  history.push("/");
};

export const editStream = (id, streamUpdates) => async (dispatch) => {
  const response = await streams.patch(`/${id}`, streamUpdates);

  dispatch({
    payload: response.data,
    type: EDIT_STREAM,
  });
  history.push("/");
};

export const fetchStream = (id) => async (dispatch) => {
  try {
    const response = await streams.get(`/${id}`);

    dispatch({
      payload: response.data,
      type: FETCH_STREAM,
    });
  } catch (error) {
    history.push("/");
  }
};

export const fetchStreams = () => async (dispatch) => {
  const response = await streams.get("");

  dispatch({
    payload: response.data,
    type: FETCH_STREAMS,
  });
};
