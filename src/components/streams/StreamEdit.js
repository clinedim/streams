import _ from "lodash";
import React, { useEffect } from "react";
import { connect } from "react-redux";

import { editStream, fetchStream } from "../../actions/streams";
import StreamForm from "./StreamForm";

const StreamEdit = ({ editStream, fetchStream, match, stream }) => {
  const { id: streamId } = match.params;

  useEffect(() => {
    fetchStream(streamId);
  }, [fetchStream, streamId]);

  const onSubmit = (formValues) => {
    editStream(streamId, formValues);
  };

  if (!stream) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <h3>Edit a Stream</h3>
      <StreamForm
        initialValues={_.pick(stream, ["description", "title"])}
        onSubmit={onSubmit}
      />
    </div>
  );
};

const mapActionsToProps = { editStream, fetchStream };

const mapStateToProps = ({ streams }, { match }) => ({
  stream: streams[match.params.id],
});

export default connect(mapStateToProps, mapActionsToProps)(StreamEdit);
