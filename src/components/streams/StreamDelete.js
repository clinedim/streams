import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { deleteStream, fetchStream } from "../../actions/streams";
import Modal from "../Modal";
import history from "../../history";

const StreamDelete = ({ deleteStream, fetchStream, match, stream }) => {
  const { id: streamId } = match.params;

  useEffect(() => {
    fetchStream(streamId);
  }, [fetchStream, streamId]);

  const actions = (
    <React.Fragment>
      <Link className="ui button" to="/">
        Cancel
      </Link>
      <button
        className="ui negative button"
        onClick={() => deleteStream(streamId)}
      >
        Delete
      </button>
    </React.Fragment>
  );

  const renderContent = () => {
    if (!stream) {
      return "Are you sure you want to delete this stream?";
    }

    return `Are you sure you want to delete the stream with title: "${stream.title}"`;
  };

  console.log(stream);

  return (
    <Modal
      actions={actions}
      content={renderContent()}
      onDismiss={() => history.push("/")}
      title="Delete Stream"
    />
  );
};

const mapActionsToProps = { deleteStream, fetchStream };

const mapStateToProps = ({ streams }, { match }) => ({
  stream: streams[match.params.id],
});

export default connect(mapStateToProps, mapActionsToProps)(StreamDelete);
