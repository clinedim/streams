import flv from "flv.js";
import React, { useEffect, useRef } from "react";
import { connect } from "react-redux";
import { fetchStream } from "../../actions/streams";

const StreamShow = ({ fetchStream, match, stream }) => {
  let flvPlayer;
  const videoRef = useRef();
  const { id: streamId } = match.params;

  useEffect(() => {
    fetchStream(streamId);
    buildPlayer();

    return () => {
      flvPlayer.destroy();
    };
  }, [fetchStream, streamId]);

  const buildPlayer = () => {
    if (flvPlayer || !stream) {
      return;
    }

    flvPlayer = flv.createPlayer({
      type: "flv",
      url: `http://localhost:8000/live/${streamId}.flv`,
    });
    flvPlayer.attachMediaElement(videoRef.current);
    flvPlayer.load();
  };

  if (!stream) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <video controls={true} ref={videoRef} style={{ width: "100%" }} />
      <h1>{stream.title}</h1>
      <h5>{stream.description}</h5>
    </div>
  );
};

const mapActionsToProps = { fetchStream };

const mapStateToProps = ({ streams }, { match }) => ({
  stream: streams[match.params.id],
});

export default connect(mapStateToProps, mapActionsToProps)(StreamShow);
