import React from "react";
import { Field, reduxForm } from "redux-form";

class StreamForm extends React.Component {
  onSubmit = (formValues) => {
    this.props.onSubmit(formValues);
  };

  renderError({ error, touched }) {
    if (touched && error) {
      return (
        <div className="ui error message">
          <div className="header">{error}</div>
        </div>
      );
    }
  }

  renderInput = ({ id, input, label, meta }) => {
    const className = `field${meta.error && meta.touched ? " error" : ""}`;

    return (
      <div className={className}>
        <label htmlFor={id}>{label}</label>
        <input autoComplete="off" id={id} {...input} />
        {this.renderError(meta)}
      </div>
    );
  };

  render() {
    console.log(this.props);

    return (
      <form
        className="ui form error"
        onSubmit={this.props.handleSubmit(this.onSubmit)}
      >
        <Field
          component={this.renderInput}
          id="title"
          label="Enter Title"
          name="title"
        />
        <Field
          component={this.renderInput}
          id="description"
          label="Enter Description"
          name="description"
        />
        <button className="ui button primary">Submit</button>
      </form>
    );
  }
}

const validate = (formValues) => {
  const errors = {};

  if (!formValues.description) {
    errors.description = "You must enter a description";
  }

  if (!formValues.title) {
    errors.title = "You must enter a title.";
  }

  return errors;
};

export default reduxForm({ form: "streamForm", validate })(StreamForm);
