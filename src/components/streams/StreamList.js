import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { fetchStreams } from "../../actions/streams";

const StreamList = ({ currentUserId, fetchStreams, isSignedIn, streams }) => {
  useEffect(() => {
    fetchStreams();
  }, [fetchStreams]);

  const renderAdmin = (stream) => {
    if (stream.userId === currentUserId) {
      return (
        <div className="right floated content">
          <Link className="ui button primary" to={`/streams/edit/${stream.id}`}>
            Edit
          </Link>
          <Link
            className="ui button negative"
            to={`/streams/delete/${stream.id}`}
          >
            Delete
          </Link>
        </div>
      );
    }
  };

  const renderCreate = () => {
    if (isSignedIn) {
      return (
        <div style={{ textAlign: "right" }}>
          <Link className="ui button primary" to="/streams/new">
            Create Stream
          </Link>
        </div>
      );
    }
  };

  const renderStreams = () =>
    streams.map((stream) => (
      <div className="item" key={stream.id}>
        {renderAdmin(stream)}
        <i className="large middle aligned icon camera" />
        <div className="content">
          <div className="header">
            <Link to={`/streams/${stream.id}`}>{stream.title}</Link>
          </div>
          <div className="description">{stream.description}</div>
        </div>
      </div>
    ));

  return (
    <div>
      <h2>Streams</h2>
      <div className="ui celled list">{renderStreams()}</div>
      {renderCreate()}
    </div>
  );
};

const mapActionsToProps = { fetchStreams };

const mapStateToProps = ({ authentication, streams }) => ({
  isSignedIn: authentication.isSignedIn,
  streams: Object.values(streams),
  currentUserId: authentication.userId,
});

export default connect(mapStateToProps, mapActionsToProps)(StreamList);
