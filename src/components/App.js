import React from "react";
import { Route, Router, Switch } from "react-router-dom";

import Header from "./Header";
import history from "../history";
import StreamCreate from "./streams/StreamCreate";
import StreamDelete from "./streams/StreamDelete";
import StreamEdit from "./streams/StreamEdit";
import StreamList from "./streams/StreamList";
import StreamShow from "./streams/StreamShow";

const App = () => {
  return (
    <div className="ui container">
      <Router history={history}>
        <Header />
        <Switch>
          <Route component={StreamList} exact path="/" />
          <Route component={StreamCreate} path="/streams/new" />
          <Route component={StreamEdit} path="/streams/edit/:id" />
          <Route component={StreamDelete} path="/streams/delete/:id" />
          <Route component={StreamShow} path="/streams/:id" />
        </Switch>
      </Router>
    </div>
  );
};

export default App;
